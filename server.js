require('marko/node-require'); // Allow Node.js to require and load `.marko` files

var express = require('express');
var markoExpress = require('marko/express');
var template = require('./template/template');

var data = {
    date: "Декабрь 2017 г.",
    tabNum: "99999",
    department: "999",
    accruals: [
        {
            code: 105,
            text: "Oсновная заработная плата (22/176)",
            sum: "8428,57"
        },
        {
            code: 666,
            text: "Пocoбиe зa дни нетрудоспособности (4/32)",
            sum: "99,99"
        }
    ]
};

require('marko/node-require').install();
require('marko/express'); //enable res.marko

var express = require('express');

var template = require('./template/template');
var app = express();
var port = 8080;



// Configure lasso to control how JS/CSS/etc. is delivered to the browser
require('lasso').configure({
    plugins: [
        'lasso-marko' // Allow Marko templates to be compiled and transported to the browser
    ]
});


app.use(require('lasso/middleware').serveStatic());

app.get('/', function(req, res) {
    res.marko(template, data);
});

app.listen(port, function() {
    console.log('Server started! Try it out:\nhttp://localhost:' + port + '/');

    if (process.send) {
        process.send('online');
    }
});