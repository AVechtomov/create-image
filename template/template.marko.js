// Compiled using marko@4.13.8 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/src/html").t(__filename),
    marko_componentType = "/create-image$1.0.0/template/template.marko",
    components_helpers = require("marko/src/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_helpers = require("marko/src/runtime/html/helpers"),
    marko_escapeXml = marko_helpers.x,
    marko_forEach = marko_helpers.f;

function render(input, out, __component, component, state) {
  var data = input;

  out.w("<h2>Расчетный лист за " +
    marko_escapeXml(data.date) +
    "</h2><h3 class=\"example\">Таб. №" +
    marko_escapeXml(data.tabNum) +
    "</h3><h3>Подразделение №" +
    marko_escapeXml(data.department) +
    "</h3>");

  if (data.accruals.length) {
    out.w("<table border=\"1\" width=\"400px\"><thead><tr><td colspan=\"3\">1.Начисления</td></tr></thead><tbody><tr><td>Вид</td><td>Наименование</td><td>Сумма</td></tr>");

    var for__12 = 0;

    marko_forEach(data.accruals, function(item) {
      var keyscope__13 = "[" + ((for__12++) + "]");

      out.w("<tr><td>" +
        marko_escapeXml(item.code) +
        "</td><td>" +
        marko_escapeXml(item.text) +
        "</td><td>" +
        marko_escapeXml(item.sum) +
        "</td></tr>");
    });

    out.w("</tbody></table>");
  }
}

marko_template._ = marko_renderer(render, {
    ___implicit: true,
    ___type: marko_componentType
  });

marko_template.Component = marko_defineComponent({}, marko_template._);

marko_template.meta = {
    deps: [
      {
          type: "css",
          code: ".count {\r\n        color:#09c;\r\n        font-size:3em;\r\n    }\r\n    .example {\r\n        color: red;\r\n    }",
          virtualPath: "./template.marko.css",
          path: "./template.marko"
        }
    ],
    id: "/create-image$1.0.0/template/template.marko"
  };
